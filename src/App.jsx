import { useState } from 'react'
import './App.css'
import { counterSlice, decrement, increment } from './counter/counterSlice'
import { connect, useDispatch, useSelector } from 'react-redux'
import Modal from './components/Modal'

function App(props) {
  // const {counter} = useSelector(state => state)
  // const dispatch = useDispatch()
  // console.log(dispatch);
  const {counter, increment, decrement } = props
  const [inputValue, setInputValue] = useState(0)
  // console.log(inputValue);
  
  return (
    <>
    <button onClick={increment}>Increment</button>
    <button onClick={decrement}>Decrement</button>
      <h1>{counter.count}</h1>
      <Modal/>
    </>
  )
}
function mapStateToProps(state) {
  return state
}
function mapDispatchToProps(dispatch) {
  return {
    increment: () => dispatch({type:"counter/increment"}),
    decrement: () => dispatch({type:"counter/decrement"})
  }
}
// export default connect((state) => state, mapDispatchToProps)(App)
export default connect(mapStateToProps, mapDispatchToProps)(App)
