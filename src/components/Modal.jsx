import React, { useReducer } from "react";


const modalReducer = (state, action) => {
  switch (action.type) {
    case "openModal":
      return { ...state, isOpen: true };
    case "closeModal":
      return { ...state, isOpen: false };
    default:
      return state;
  }
};

const Modal = () => {
  const initialState = {
    isOpen: false,
  };

  const [modalState, dispatch] = useReducer(modalReducer, initialState);

  const openModal = () => {
    dispatch({ type: "openModal" });
  };

  const closeModal = () => {
    dispatch({ type: "closeModal" });
  };

  return (
    <div>
        <h1>Modal</h1>
      <button onClick={openModal}>Show</button>
      <button onClick={closeModal}>Hide</button>
      {modalState.isOpen && (
        <div>
          <div>
            <p>Lorem ipsum dolor sit amet.</p>
          </div>
        </div>
      )}
    </div>
  );
};

export default Modal;
